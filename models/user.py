from db import db


"""
User Class: 
    - Base client for application
    - Provides an interface for the interaction it has with the user
"""
class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))

    def __init__(self, username, password):
        """
        objective: creates a new user object
        :param username: str - user name
        :param password: str - user password
        """
        self.username = username
        self.password = password

    def save_to_db(self):
        """
        objective: save user to database
        :return: None
        """
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        """
        objective: searches the database by user name
        :param username: str - user name
        :return: New User object, if exists otherwise None
        """
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        """
        objective: searches the database by user id
        :param _id: int - unique user identifier
        :return: New User object if exists, otherwise None
        """
        return cls.query.filter_by(id=_id).first()