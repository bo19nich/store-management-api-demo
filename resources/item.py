from models.item import ItemModel
from flask_restful import Resource, reqparse
from flask_jwt import jwt_required


"""
Item Class:
    - Responsible for handling requests for a mock store
      that contains item and pricing information.
"""
class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('price',
                        type=float,
                        required=True,
                        help='This field cannot be left blank.')
    parser.add_argument('store_id',
                        type=int,
                        required=True,
                        help='Every item needs a store id.')

    @jwt_required()
    def get(self, name):
        """
        objective: GET an item from item database.
        :param name: str - item name
        :return: json - item with status code
        """
        item = ItemModel.find_by_name(name)
        return item.json() if item else {'message': f'An item with the name {name} is not found.'}, 404

    def post(self, name):
        """
        objective: POST an new item to item database.
        :param name: str - new item name
        :return: json - new item with status code
        """
        if ItemModel.find_by_name(name):
            return {'message': f'An item with the name {name} already exists.'}, 400

        data = Item.parser.parse_args()

        # equivalent to **data = data['price'], data['store_id']
        item = ItemModel(name, **data)
        try:
            item.save_to_db()
        except:
            return {'message': 'Item failed to store to database.'}, 500
        return item.json(), 201

    def delete(self, name):
        """
        objective: DELETE an item from the item database.
        :param name: str - item name
        :return: json - status message
        """
        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()
        return {'message': 'Item deleted.'}

    def put(self, name):
        """
        objective: PUT an item into the in-memory database store. If the
                   item exists, update its contents.
        :param name: str - item name
        :return: json - item
        """
        data = Item.parser.parse_args()
        item = ItemModel.find_by_name(name)

        # create a new item if nonexistent in db
        if item is None:
            item = ItemModel(name, **data)
        # otherwise just modify the contents if its price
        else:
            item.price = data['price']
        item.save_to_db()
        return item.json()


"""
Class ItemList:
    - Responsible for requests that pertain to the entire 
      list of items in the database (in this case every row).
"""
class ItemList(Resource):
    def get(self):
        """
        objective: return in json format, every item in database
        :return: json - db
        """
        return {'items': [item.json() for item in ItemModel.query.all()]}