from models.user import UserModel
from flask_restful import reqparse
from flask_restful import Resource


"""
UserRegister Class:
    - Responsible for handling requests related to user registration
    - /register endpoint
"""
class UserRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('username', type=str, required=True)
    parser.add_argument('password', type=str, required=True)

    def post(self):
        """
        objective: receives username and password from client and
                   inserts the information into the database.
        :return: json - message status code
        """
        data = UserRegister.parser.parse_args()
        if UserModel.find_by_username(data['username']):
            return {'message': 'A user with that username already exists.'}, 400

        user = UserModel(**data)
        user.save_to_db()

        return {'message': f'{user.username} was successfully registered.'}, 201