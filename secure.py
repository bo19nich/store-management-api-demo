from werkzeug.security import safe_str_cmp
from resources.user import UserModel


def authenticate(username, password):
    """
    objective: ensure user exists in database, and that
               their pass words match.
    :param username: str - user name
    :param password: str - user passowrd
    :return: User object if authenticated, otherwise None
    """
    user = UserModel.find_by_username(username)
    if user and safe_str_cmp(user.password, password):
        return user


def identity(payload):
    """
    objective: identify the identity of user through
               the passed in payload
    :param payload: dict
    :return: int - user it
    """
    user_id = payload['identity']
    return UserModel.find_by_id(user_id)